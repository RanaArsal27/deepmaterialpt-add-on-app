#DeepMaterialPT Solutions

[DeepmaterialPT](https://www.deepmaterialpt.com/) provide customized services on your demand, custom electronic adhesives, PUR structural adhesive, UV moisture curing adhesive, epoxy adhesive, conductive silver glue, epoxy underfill adhesive, epoxy encapsulant, functional protective film, semiconductor protective film.

Deepmaterial Adhesives specializes in adhesives for the plastic, glass, metal products industry. We offer a full range of products for almost every imaginable application within this market. Below you will find a number of different adhesive categories and a description of the available products within each category.

Plastic is a very flexible and durable material, perfect for a variety of home projects. However, it can be difficult to find glues for these projects because many common glues don't work well with plastics. That's because many kinds of plastic have extremely smooth and glossy surfaces. Their lack of roughness and porosity makes it difficult for adhesives to find anything to bond to. Fortunately, however, there are some common adhesives on the market—some designed specifically for plastics, some not—that will get the job done.

One- and two-part Deepmaterial metal/glass binder compounds have excellent strength properties. Available in a range of viscosities and cure rates, these products adhere soda lime glass, borosilicate glass, fused silica glass, and aluminosilicate glass to metals such as aluminum, titanium, copper, steel, cast iron, and invar. Differences in thermal expansion coefficients need special consideration to ensure the correct adhesive is selected.
